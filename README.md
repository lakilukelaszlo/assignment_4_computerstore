# Computer Store - A Dynamic Webpage using JavaScript

This is a dynamic webpage using "vanilla" JavaScript. 

This repository contains:

1. The [HTML entry point](index.html).
2. A [CSS style sheet](styles.css).
3. A folder with [JavaScript files](js/).


## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)


## Background
This webpage simulates a store for purchasing laptops. A user can work and get a loan to then use the money to purchase a laptop.

## Install
Clone repository via `git clone`. You can use Visual Studio Code to locally run the website.

## Usage
This webpage contains following sections:
- Bank
- Work
- Laptop Selection
- Laptop Description



## Maintainers

[@Laszlo Laki](https://gitlab.com/lakilukelaszlo).

