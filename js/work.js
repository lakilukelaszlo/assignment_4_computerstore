class Work {
    #pay;
    payElement;
    #repayLoanButton;
    #bank;
    amount;
    formatter;
  
    constructor(payElement, bank, repayLoanButton, formatter) {
      this.#pay = 0;
      this.payElement = payElement;
      this.#repayLoanButton = repayLoanButton;
      this.#bank = bank;
      this.amount = 100;
      this.formatter = formatter;
    }
  
    hideRepayLoanButton() {
      this.#repayLoanButton.style.display = "none"
    }

    // Getter

    getPay() {
      return this.#pay;
    }
  
    // Setter

    setPay(newPay) {
      this.#pay = newPay;
      this.updatePayElement();
    }

    // Methods
  
    repayLoan() { 
      if (this.#bank.getLoan() > this.getPay()) {
        this.#bank.setLoan(this.#bank.getLoan() - this.getPay());
        this.setPay(0);
      } 
      else {
        this.setPay(this.getPay() - this.#bank.getLoan());
        this.#bank.setLoan(0);
        this.hideRepayLoanButton()
        this.#bank.setBalance(this.#bank.getBalance() + this.getPay())
        this.setPay(0)
      }
    }
  
    bankTransfer() {
        const repayment = this.getPay() * 0.1;
        let deposit = this.getPay() - repayment;
        this.setPay(0)
        if (this.#bank.getLoan() > repayment) {
            console.log("this.#bank.getLoan() > repayment")
            this.#bank.setLoan(this.#bank.getLoan() - repayment); 
        }
        else if (this.#bank.getLoan() <= repayment) {
            console.log("this.#bank.getLoan() <= repayment")
            deposit += (repayment - this.#bank.getLoan());
            this.#bank.setLoan(0);
            this.hideRepayLoanButton();
        }
        else {
            console.log("Both conditions did not apply")
        }
        this.#bank.setBalance(this.#bank.getBalance() + deposit);
    }

    updatePayElement() {
      this.payElement.innerText = `Pay: ${this.formatter.format(this.getPay())}`;
    }
  
    receivePayment() {
      this.setPay(this.#pay + this.amount);
    }
  }

export default Work