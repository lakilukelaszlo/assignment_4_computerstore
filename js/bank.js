class Bank {
    #balance;
    #loan;
    loanElement;
    balanceElement;
    formatter;
    repayLoanButton;
  
    constructor(balanceElement, loanElement, formatter, repayLoanButton) {
      this.#balance = 0; 
      this.#loan = 0;
      this.loanElement = loanElement;
      this.balanceElement = balanceElement;
      this.formatter = formatter;
      this.repayLoanButton = repayLoanButton;
    }
  
    // Getters

    getBalance() {
      return this.#balance;
    }
  
    getLoan() {
      return this.#loan;
    }
  
    // Setters

    setBalance(newBalance) {

      this.#balance = newBalance;
     
      this.updateBalanceElement();
    }
  
    setLoan(newLoan) {
    
      this.#loan = newLoan;
    
      this.updateLoanElement();
    }
    
    // Methods

    updateLoanElement() {
      this.loanElement.innerText = `Outstanding loan: ${this.formatter.format(
        this.getLoan()
      )}`;
      if (this.getLoan() === 0) {
      
        this.loanElement.style.display = "none"
      }
      
      else {
        this.loanElement.style.display = "inline-block"
      }
    }

    updateBalanceElement() {
      this.balanceElement.innerText = `Balance: ${this.formatter.format(this.getBalance())}`;
    }

    receiveLoan() {
      if (this.getLoan() > 0) {
        alert("You must pay back your loan before getting a new one!");
      } else {
        let loanInput = prompt("Enter the amount of the loan");
        if (isNaN(loanInput)) {
          alert("Enter a number!")
        }
        else {
          loanInput = Math.round(loanInput * 100) / 100;
          if (loanInput <= 0) {
            alert("Your loan must be greater than zero!")
          }
          else if (loanInput > 2 * this.getBalance()) {
            alert("You cannot get a loan more than double of your bank balance.");
          } else {
            this.setLoan(loanInput);
            this.setBalance(this.getBalance() + this.getLoan())
            this.repayLoanButton.style.display = "inline-block"; 
          }
        }
      }
    }
  }

  export default Bank