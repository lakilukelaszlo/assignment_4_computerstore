import Work from "./work.js"
import Bank from "./bank.js"


async function fetchLaptops() { 
  try {
    const response = await fetch(
      urlLaptopEndpoint
    );
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(`Error: ${error.message}`);
  }
}


const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("remaining_loan");
const getLoanButton = document.getElementById("get_loan");
const payElement = document.getElementById("pay");
const bankButton = document.getElementById("bank");
const workButton = document.getElementById("work");
const repayLoanButton = document.getElementById("repay_loan");
repayLoanButton.style.display = "none";
const laptopFeaturesHeader = document.getElementById("features");
laptopFeaturesHeader.style.display = "none";
const laptopFeaturesElement = document.getElementById("laptop_features");
const laptopSelectElement = document.getElementById("laptop");
const laptopImageElement = document.getElementById("laptop_image");
const laptopTitleElement = document.getElementById("laptop_title");
const laptopDescriptionElement = document.getElementById("laptop_description");
const priceElement = document.getElementById("price");
const buyNowButton = document.getElementById("buy_now");


const formatter = formatToCurrency();
const urlLaptopBase = "https://noroff-komputer-store-api.herokuapp.com/"
const urlLaptopEndpoint = `${urlLaptopBase}computers`
const bank = new Bank(balanceElement, loanElement, formatter, repayLoanButton);
const work = new Work(payElement, bank, repayLoanButton, formatter);
let displayedLaptop = "";

const laptops = await fetchLaptops();


function onSelectChange() {
  const laptopId = laptopSelectElement.value;
  displayedLaptop = laptops.find((targetLaptop) => parseInt(targetLaptop.id) === parseInt(laptopId)); 
  renderLaptop();
}

function onImageNotFound() {
  laptopImageElement.src = "image_not_found";
}

function onGetLoan() {
  bank.receiveLoan();
}

function onWork() {
  work.receivePayment();
}

function onRepayLoan() {
  work.repayLoan();
}

function onBank() {
  work.bankTransfer();
}


function updateLaptopOptions() {
  for (const laptop of laptops) {
    laptopSelectElement.innerHTML += `<option value=${laptop.id}>${laptop.title}</option>`;
  }
}

function formatToCurrency() {
  return Intl.NumberFormat("en-GB", {
    style: "currency",
    currency: "EUR",
  });
}

function onBuyNow() {
  if (displayedLaptop.price === undefined) {
    alert("You should select a laptop!")
  }
  else if (bank.getBalance() >= displayedLaptop.price) {
    alert("You purchased the laptop sucessfully")
    bank.setBalance(bank.getBalance() - displayedLaptop.price)
 }
  else {
  alert("You cannot afford this laptop!")
 }
}

const renderLaptop = () => {
  laptopImageElement.addEventListener("error", onImageNotFound);
  laptopImageElement.src = urlLaptopBase + displayedLaptop.image; 
  laptopTitleElement.innerText = displayedLaptop.title;
  laptopDescriptionElement.innerText = displayedLaptop.description;
  priceElement.innerText = `${formatter.format(displayedLaptop.price)}`; 
  laptopFeaturesHeader.style.display = "inline-block";
  laptopFeaturesElement.innerHTML = "";
  for (const feature of displayedLaptop.specs) {
    laptopFeaturesElement.innerHTML += `
    <li>${feature}</li>`
  }
};


bank.updateBalanceElement();
bank.updateLoanElement();
work.updatePayElement();
updateLaptopOptions();

laptopSelectElement.addEventListener("change", onSelectChange);
getLoanButton.addEventListener("click", onGetLoan);
workButton.addEventListener("click", onWork);
repayLoanButton.addEventListener("click", onRepayLoan);
bankButton.addEventListener("click", onBank);
buyNowButton.addEventListener("click", onBuyNow)
